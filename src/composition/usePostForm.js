export default function usePostForm(post) {
    function setText(val) {
        post.value.text = val;
    }

    function setUserId(val) {
        post.value.userId = val;
    }

    return {
        setText,
        setUserId,
    }
}