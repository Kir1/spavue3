import { ref, computed, provide } from 'vue';
import axios from 'axios';

export default function useUsersTable() {
    const requestStatus = {
        'success': 'success',
        'error': 'error',
        'loaded': 'loaded'
    };

    const users = ref([]);
    const modal = ref(null);
    const postIdCounter = ref(0);
    const resultRequest = ref(requestStatus.loaded);
    const loadedStatusText = computed(() => {
        return resultRequest.value === requestStatus.loaded ? 'Загрузка...' : 'Произошла ошибка. Пожалуйста, перезагрузите страницу.'
    })

    function fetchUserAndPosts() {
        axios.get('https://jsonplaceholder.typicode.com/users').then(responseUsers => {
            const usersData = {};
            const getPostsRequests = responseUsers.data.map(user => {
                usersData[user.id] = { ...user };
                return axios.get(`https://jsonplaceholder.typicode.com/users/${user.id}/posts`)
            });
            Promise.all(getPostsRequests).then((userPosts) => {
                const postIds = [];
                users.value = userPosts.map(posts => {
                    return {
                        ...usersData[posts.data[0].userId],
                        posts: posts.data.map(p => {
                            postIds.push(p.id);
                            return { text: p.title, id: p.id, userId: p.userId };
                        }),
                    }
                });
                postIdCounter.value = Math.max(...postIds);
                resultRequest.value = requestStatus.success;
            }).catch(() => {
                resultRequest.value = requestStatus.error;
            });
        }).catch(() => {
            resultRequest.value = requestStatus.error;

        })
    }

    function addPost(post) {
        postIdCounter.value++;
        const foundUser = users.value.find(u => Number(u.id) == Number(post.userId));
        foundUser.posts.unshift({
            ...post,
            id: postIdCounter.value,
            //newPost: true //если бы бекенд использовал одно апи для создания и редактир постов
        });
        closeModal();
    }

    function updatePost(post) {
        const user = modal.value.user;
        const postsLink = user.posts;
        const userId = user.id;
        const foundIndex = postsLink.findIndex(p => Number(p.id) == Number(post.id));

        if (Number(userId) === Number(post.userId)) {
            postsLink[foundIndex] = { ...post };
        } else {
            removePost(user, post.id);
            const foundUser = users.value.find(u => Number(u.id) == Number(post.userId));
            foundUser.posts.unshift({ ...post, id: postIdCounter.value });
        }
        closeModal();
    }

    function removePost(user, postId) {
        user.posts = user.posts.filter(post => post.id !== postId);
    }

    function openModal(newModal) {
        modal.value = newModal;
    }

    function closeModal() {
        modal.value = null;
    }

    provide('users', {
        users,
    })

    provide('post', {
        addPost,
        updatePost,
        removePost,
    })

    provide('modal', {
        modal,
        openModal,
        closeModal,
    })

    return {
        users,
        requestStatus,
        loadedStatusText,
        fetchUserAndPosts,
        resultRequest
    }
}