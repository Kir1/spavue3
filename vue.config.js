const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import "./src/scss/variables.scss"; @import "./src/scss/main.scss";`
      },
    }
  }
})
